from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ToDoItem(models.Model):
	task_name = models.CharField(max_length=50)
	description = models.CharField(max_length=200)
	status = models.CharField(max_length=50, default="pending")
	date_created = models.DateTimeField('date created')
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")


# models.CharField() = string
# models.DateTimeField() = dateTime
# max_length=*value* sets the max amount of characters for the variable	
# default="value" sets the default value of the variable if no input was added
